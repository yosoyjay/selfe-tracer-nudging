"""
Methods to handle Pat's data for the biological model

Jesse Lopez - 01/27/2013
"""
#-------------------------------------------------------------------------------
# Imports
#-------------------------------------------------------------------------------
import sys
import numpy as np
from mesh3D import *
from nudge import nudgeFile as nf

#-------------------------------------------------------------------------------
# Constants 
#-------------------------------------------------------------------------------
# XXX: Slightly arbitrary may have to add/subtract and play with these to get 
#      the interpolation correct
depths = np.array([-30, -20, -7.5, -2.5])

#-------------------------------------------------------------------------------
# Functions and such
#-------------------------------------------------------------------------------
def loadPatsData(file):
  """Loads Pat MATLAB data files and reshape for interpolation

  Pat sez:
  <5m
  5-10m
  10-30m
  >30m

  Parameters:
  -----------
    file - Path to Pat's data
  
  Returns:
  --------
    data - Dictionary of concentrations (day, depth) with tracer number as key
  """
  data = io.loadmat(file)
  reshaped_data = {} 
  print 'Data read in from MATLAB file:'
  for var in data.keys():
    if var[0] == '_':
      continue 
    print var
    day = var[4] 
    reshaped_data[day] = data[var] 

  return reshaped_data

def setup_tracer_files(datafile, hgrid, vgrid):
  """Creates trcr_n_nu.in files for nudging tracers. 

  Parameters:
  -----------
    datafile - Path to Pat's MATLAB data file
    hgrid - Path to hgrid file for run
    vgrid - Path to vgrid file for run
  """
  tracers = loadPatsData(datafile)
  n_days = tracers['1'].shape[0]
  nudging_file = nf.fromGrids('', hgrid, vgrid, n_days)
  grid = nudging_file.mesh

  # Create one file at a time to avoid memory issues 
  for key in tracers.keys():
    file = 'trcr_%s_nu.in' % key
    nudging_file.file = file
    data = [] 
    for day in np.arange(n_days):
      grid.interpVertically(depths, tracers[key][day,::-1]) 
      data.append(grid.data)
    nudging_file.data = data
    nudging_file.writeFile()

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "Usage: %s data.mat hgrid.gr3 vgrid.in" % sys.argv[0]
	else:
		setup_tracer_files(sys.argv[1], sys.argv[2], sys.argv[3])
