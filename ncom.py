"""
Read NCOM files

Jesse Lopez - 01/23/2013
"""

#-------------------------------------------------------------------------------
# Imports
#-------------------------------------------------------------------------------
import netCDF4
import numpy as np
import sys

#-------------------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------------------
if sys.platform == 'darwin':
  nc_h ='/Users/jesse/Dropbox/projs/tracer_nudge/model_h.nc'
  nc_lat = '/Users/jesse/Dropbox/projs/tracer_nudge/model_lat.nc'
  nc_lon = '/Users/jesse/Dropbox/projs/tracer_nudge/model_lon.nc'
  nc_zm = '/Users/jesse/Dropbox/projs/tracer_nudge/model_zm.nc'
else:
  nc_h = '/home/workspace/ccalmr6/nrldata/model_h.nc'
  nc_lat = '/home/workspace/ccalmr6/nrldata/model_lat.nc'
  nc_lon = '/home/workspace/ccalmr6/nrldata/model_lon.nc'
  nc_zm = '/home/workspace/ccalmr6/nrldata/model_zm.nc'


#-------------------------------------------------------------------------------
# Functions and such 
#-------------------------------------------------------------------------------
class ncomData(object):
  x_index = None
  y_index = None
  lat = None
  lon = None
  zm = None
  init = False 

  def __init__(self, x_index, y_index, data, name, level):
    if ncomData.init == False:
      self.loadNCOMConsts()

    self.x_index = x_index
    self.y_index = y_index
    self.level = level
    self.data = data
    self.name = name

  @classmethod 
  def fromFile(cls, file_path):
    """Loads data from an NCOM.nc file"""
    f = netCDF4.Dataset(file_path) 
    vars = f.variables.keys() 
    # TODO: Move to dict comprehension 
    data = {}
    std_var = ['X_Index', 'Y_Index', 'index', 'level']
    for var in vars:
      data[var] = f.variables[var][:]
      if not var in std_var:
        name = var

    return cls(data['X_Index'], data['Y_Index'], data[name], name, data['level']) 

  def loadNCOMConsts(self):
    """Loads static NCOM model data"""
    f = netCDF4.Dataset(nc_h)
    print 'Loading X and Y Indices'
    self.__class__.x_index = f.variables['X_Index'][:]
    self.__class__.y_index = f.variables['Y_Index'][:]
    f.close()

    f = netCDF4.Dataset(nc_lat)
    print 'Loading latitudes'
    self.__class__.lat = f.variables['Lat'][:]
    f.close()

    f = netCDF4.Dataset(nc_lon)
    print 'Loading longitudes'
    self.__class__.lon = f.variables['Long'][:]
    f.close()

    f = netCDF4.Dataset(nc_zm)
    print 'Loading zm'
    self.__class__.zm = f.variables['zm'][:]
    f.close()

    self.__class__.init = True

