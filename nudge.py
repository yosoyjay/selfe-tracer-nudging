"""
Methods to handle nudging input files for SELFE.

Jesse Lopez - 01/24/2013
"""

#-------------------------------------------------------------------------------
# Imports 
#-------------------------------------------------------------------------------
import mesh3D as mi 
import gr3Interface as gr3
import numpy as np

#-------------------------------------------------------------------------------
# Functions and such 
#-------------------------------------------------------------------------------
class nudgeFile(object):
  def __init__(self, fileName, freq=None, mesh=None, nDays=None, 
               data=None, nNodes=None, nvrt=None): 
    self.file = fileName
    self.freq = freq
    self.mesh = mesh 
    self.nDays = nDays
    self.data = data 
    self.nNodes = nNodes 
    self.nvrt = nvrt 

  @classmethod
  def fromGrids(cls, fileName, gr3File, vGridFile, days, freq=86400):
    """Create a nudging file object for supplied grids
   
    Parameters:
      fileName - Nudging file name
      gr3File - hgrid.gr3 file path 
      vGridFile - vgrid.in file path
      days - Length of nudging file
      freq - Frequency to impose nudging
   
    Returns:
      nudgeFile object
    """
    hgrid = gr3.readGR3File(gr3File)
    vgrid = gr3.verticalCoordinates.fromVGridFile(vGridFile)
    mesh = mi.mesh3D.fromGR3Interface(hgrid, vgrid)
    nNodes = hgrid.x.shape[0]
    nvrt = vgrid.nvrt

    return cls(fileName, freq=86400, mesh=mesh, nDays=days, 
               nNodes=nNodes, nvrt=nvrt)

  @classmethod
  def fromNudgingFile(cls, fileName, gr3File, vGridFile):
    """Big assumption that the frequency is 86400 seconds
   
    Parameters:
      fileName - Nudging file name
      gr3File - hgrid.gr3 file path 
      vGridFile - vgrid.in file path
   
    Returns:
      nudgeFile object
    """
    hgrid = gr3.readGR3File(gr3File)
    vgrid = gr3.verticalCoordinates.fromVGridFile(vGridFile)
    mesh = mi.mesh3D.fromGR3Interface(hgrid, vgrid)
    nNodes = hgrid.x.shape[0]
    nvrt = vgrid.nvrt

    # Read nudging times (typically days) separated by node because that's
    # how SELFE expects the records to be split up.
    times = []
    dataAllDays = []
    f = open(fileName, 'rb')
    while np.fromfile(f, dtype='int32', count=1).size != 0:
      time = np.fromfile(f, dtype='float32', count=1)
      print 'Reading time %d' % time
      times.append(time)
      np.fromfile(f, dtype='int32', count=1)

      data = np.zeros((nNodes*nvrt,), dtype='float32') 
      for node in xrange(nNodes):
        np.fromfile(f, dtype='int32', count=1)
        data[nvrt*node:nvrt*(node+1)] = np.fromfile(f, dtype='float32',
                                                    count=nvrt)
        np.fromfile(f, dtype='int32', count=1)
      dataAllDays.append(data)
    f.close()

    return cls(fileName, freq=86400, mesh=mesh, nDays=len(dataAllDays), 
               data=dataAllDays, nNodes=nNodes, nvrt=nvrt )


  def writeFile(self, file=None):
    """Writes binary data to file using single precision and FORTRAN record
    bracketing around actual data.
    """
    if file is None: 
      file = self.file
    time = np.array([0], dtype='float32')
    record = np.array([0], dtype='int32')
    print 'Writing nudging file: '+file
    print 'Nodes: %d, Vertical levels: %d' % (self.nNodes, self.nvrt)
    dType = self.data[0][0].dtype
    if dType != np.float32: 
      print 'WARNING: data is being cast to float32 for writing'
    f = open(file, 'wb')
    for d in xrange(self.nDays):
      time[0] = d*self.freq
      record[0] = 4
      print 'Writing time %d' % time[0]
      record.tofile(f)
      time.tofile(f)
      record.tofile(f)
      
      data = self.data[d]
      for n in xrange(self.nNodes):
        record[0] = self.nvrt*4
        record.tofile(f)
        data[n*self.nvrt:(n+1)*self.nvrt].astype('float32').tofile(f)
        record.tofile(f)
    f.close()

#  Need method to swap out data like from NCOM?
#  def loadData(self, mesh3D):
#    """Basic idea to load new data for each iteration""" 
#    self.mesh = mesh3D 

