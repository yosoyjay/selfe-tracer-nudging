"""
Used to interpolated data between meshes and sparse data on meshes.

Designed initially to interpolate data for creation of nudging files.

Jesse Lopez - 01/24/2013
"""
#-------------------------------------------------------------------------------
# Imports
#-------------------------------------------------------------------------------
import scipy as sp
import numpy as np
import scipy.io as io
from scipy import interp
from scipy.interpolate import griddata, splrep, splev
import gr3Interface as gr3
from ncom import *

#-------------------------------------------------------------------------------
# Functions and such 
#-------------------------------------------------------------------------------
class mesh3D(object):
  def __init__(self, points, nodes, nvrt):
    self.points = points 
    self.data = np.zeros_like(self.points[:,0])
    self.coordSys = None
    self.nNodes = nodes
    self.nvrt = nvrt
    self.idCoordSys()

  @classmethod
  def fromGR3Interface(cls, hgrid, vgrid):
    """3D mesh object from gr3 and vertical grid objects from gr3Interface
    
    Parameters:
    -----------
      hgrid - gr3Object of hgrid data
      vgrid - verticalCoordinates of vgrid data

    Returns:
    --------
      mesh3D instance 
    """
    nNodes = hgrid.x.shape[0]
    points = np.zeros((nNodes*vgrid.nvrt,3))

    # depths are bathymetric bottom, depth of bottom of grid.
    # vc are depths at each level relative to 0
    depths = np.squeeze(hgrid.getField('depth'))
    eta = np.zeros_like(depths)
    vc, _, _ = vgrid.computeVerticalCoordinates(eta, depths)
    points[:,2] = vc.flatten('f')
    points[:,1] = np.repeat(hgrid.y, vgrid.nvrt)
    points[:,0] = np.repeat(hgrid.x, vgrid.nvrt)

    return cls(points, nNodes, vgrid.nvrt)

  @classmethod
  def fromGR3(cls, gr3File, vGridFile):
    """3D mesh object created from hgrid.gr3 and vgrid.gr3

    Parameters:
    -----------
      gr3File - Path to hgrid.gr3
      vGridFile - Path to vgrid.in

    Returns:
    --------
      mesh3D instance 
    """
    hgrid = gr3.readGR3File(gr3File)
    vgrid = gr3.verticalCoordinates.fromVGridFile(vGridFile)
    nNodes = hgrid.x.shape[0]
    points = np.zeros((nNodes*vgrid.nvrt,3))

    # depths are bathymetric bottom, depth of bottom of grid.
    # vc are depths at each level relative to 0
    depths = np.squeeze(hgrid.getField('depth'))
    eta = np.zeros_like(depths)
    vc, _, _ = vgrid.computeVerticalCoordinates(eta, depths)

    points[:,2] = vc.flatten('f')
    points[:,1] = np.repeat(hgrid.y, vgrid.nvrt) 
    points[:,0] = np.repeat(hgrid.x, vgrid.nvrt) 

    return cls(points, nNodes, vgrid.nvrt)

  def interpSparse(self, points, data, method='nearest'):
    """Interpolates sparse input data to mesh in three-dimensions 

    Parameters:
    -----------
      points - Array like (:,3) of location data
      data - Array like (:) of data for each point
    """
    self.data = griddata(points, data, self.points, method=method)
  
  def interpVertically(self, depths, data, method='linear'):
    """Interpolates data vertically for every node using a linear interp with
    constant values for extrapolated values.
  
    Parameters:
    -----------
      depths - Array of depths
      data - Array of data 
    """
    if method=='linear':
      for n in np.arange(self.nNodes):
        start = n*self.nvrt
        end = (n+1)*self.nvrt
        self.data[start:end] = interp(self.points[start:end,2], depths, data,
                                      left=data[0], right=data[-1])
    else:
      f = splrep(depths, data)
      for n in np.arange(self.nNodes):
        start = n*self.nvrt
        end = (n+1)*self.nvrt
        self.data[start:end] = splev(self.points[start:end,2], f)

  def idCoordSys(self):
    """Identify coordinate system based on x and y data"""
    if np.abs(np.max(self.points[:,0])) > 180:
      self.coordSys = 'spcs'
    else:
      self.coordSys = 'lat/lon'

# TODO: Deal with methods to adhere to DataContainer standards
def formatLocations(x, y, z):
  """Formats location data into points to use with interpolation
  
  Parameters:
  -----------
    x - Array like x coordinates
    y - Array like y coordiantes
    z - Array like z coordinates

  Returns:
  --------
    points - Array (n,3)
  """
  return np.array([x,y,z])


#TODO: Finish this up
def mesh3DFromNCOM(ncomData):
  """Convenience method for simple interface to load NCOM data for
  interpolation to 3D mesh.

  Parameters:
  -----------
    ncomData - ncomData object with some kind of data loaded

  Returns:
  --------
    ncom3D - mesh3D object with ncom data
  """
